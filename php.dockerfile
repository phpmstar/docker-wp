# Multi-stage build
FROM php:8.0-fpm AS builder

# Install necessary packages for building PHP extensions
RUN apt-get update && \
    apt-get install -y \
        libzip-dev \
        curl \
        && docker-php-ext-install zip

# Install and enable XDebug
RUN pecl install xdebug && docker-php-ext-enable xdebug

# Build WP-CLI phar file
RUN curl -o /usr/local/bin/wp -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar && \
    chmod +x /usr/local/bin/wp

# Build Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Install necessary PHP extensions
RUN docker-php-ext-install mysqli pdo pdo_mysql && docker-php-ext-enable pdo_mysql

# Copy XDebug configuration file
COPY ./config/xdebug.ini /usr/local/etc/php/conf.d/xdebug.ini

# Final image
FROM php:8.0-fpm

# Install necessary packages for running PHP
RUN apt-get update && \
    apt-get install -y \
        libzip-dev \
        && docker-php-ext-install zip

# Install and enable XDebug
RUN pecl install xdebug && docker-php-ext-enable xdebug

# Install necessary PHP extensions
RUN docker-php-ext-install mysqli pdo pdo_mysql && docker-php-ext-enable pdo_mysql

# Copy XDebug configuration file
COPY --from=builder /usr/local/etc/php/conf.d/xdebug.ini /usr/local/etc/php/conf.d/xdebug.ini

# Copy WP-CLI binary from builder stage
COPY --from=builder /usr/local/bin/wp /usr/local/bin/wp

# Copy Composer binary from builder stage
COPY --from=builder /usr/local/bin/composer /usr/local/bin/composer
