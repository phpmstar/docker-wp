# Docker environment for Wordpress 
### What's included in this build::

* Current build of php-fpm 8.0/Nginx/MariaDB/Xdebug/WP-CLI/Composer
* self-signed certificates (for HTTPS)
* easy addition of any technology ( redis/memcached/elasticSearch e.t.c)

### Make sure you have [mkcert](https://github.com/FiloSottile/mkcert) installed before installing
after installing mkcert cd /nginx/certs and run the following command mkcert example.com (where example.com is your
domain name).

### to build containers - run ***docker-compose up*** 

### [WP-CLi](https://wp-cli.org/) command list

Telegram  ***[morn1ngstar1](https://t.me/morn1ngstar1)***